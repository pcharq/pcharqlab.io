---
layout: project
title: Livada Villas
category:
- Villa
location: Greece
size:  Plot 9120m2, Construction 1928 m2
carousel:
- professional/26/01.jpg
- professional/26/02.jpg
- professional/26/03.jpg
- professional/26/04.jpg
- professional/26/05.jpg
- professional/26/06.jpg
- professional/26/07.jpg
- professional/26/08.jpg
- professional/26/09.jpg
- professional/26/10.jpg
- professional/26/11.jpg
- professional/26/12.jpg







img: /files/frames/livada.jpg
background-img: /files/frames/livada.jpg
---

4 Villas in Livada, Greece