---
layout: project
title: Horizontes <br>Housing
category:
- Collective Housing
location: Estepona, Spain
size: Plot 3.265 m2, Housing 10.395 m2
carousel:
- professional/7/01.jpg
- professional/7/02.jpg
- professional/7/03.jpg


img: /files/frames/concurso.jpg
---

The horizontal plane of the terrain rises forming the envelope of the building. This dissipates on the horizon as an extensive garden of sinuous forms that develop in an axis from East to West.

A design is proposed that provides exclusivity to the homes, giving priority to the privacy of each one.
The envelope consists of plant covers and mostly glazed enclosures. The roofs serve as individual terraces-gardens to the houses that appear on the roof.
This large garden formed by the plant covers culminates in the community pool forming a large set that fades into the horizon.
All homes enjoy views of the sea and the mountains, with architectural spaces designed to enjoy. The interiors project a modern and elegant air with quality materials and innovative spaces that recall elements of nature and transmit tranquility.

The common spaces are distributed on the lower floors and enjoy views and natural lighting.

In spite of its block urbanistic condition, the building has been treated in its concept as an urbanization of isolated dwellings, endowing them with a sense of independence. A ramp that runs through garden areas connects the access modules and common areas.

The vehicle area is located below ground connected to the different access modules to the homes.

The building is designed to achieve the greatest sustainability by providing a balance between nature and the most innovative technologies, without forgetting the quality of the spaces and the quality in detail.