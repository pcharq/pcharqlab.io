---
layout: project
title: Banus <br>Co-Working
category:
- Public Building
location: Marbella, Spain
size: Plot 950 m2, Building 210 m2
carousel:
- professional/12/01.jpg
- professional/12/02.jpg
- professional/12/03.jpg
- professional/12/04.jpg
- professional/12/05.jpg
- professional/12/06.jpg


img: /files/frames/coworking.jpg
---

Complete refurbishemnt of an empty premises in Puerto Banús for a Co-working space.