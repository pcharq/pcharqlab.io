---
layout: project
title: Riaza flex housing
category:
- Collective Housing
location: Riaza, Spain
size: Plot 27.400 m2, Housing 26.000 m2
carousel:
- academic/1/1.jpg
- academic/1/2.jpg
- academic/1/3.jpg
- academic/1/4.jpg
- academic/1/5.jpg
img: /files/frames/flex-housing.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

In this project, the residential cell undertakes the importance and necessity of spatial flexibility so that the domestic space is as close as possible to the projection of its inhabitants within the conditions that their social and urban environment allow. 

The constructive systems used are suitable to try to approach hybrid results that admit both the use of contemporary components and vernacular solutions, capable of complementing each other, offering an easy and clear reading of the architecture through which they are expressed.