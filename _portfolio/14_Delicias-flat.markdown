---
layout: project
title: Delicias <br>flat
category:
- Flat
location: Madrid, Spain
size: 60 m2
carousel:
- professional/1/1.jpg
- professional/1/2.jpg
- professional/1/3.jpg
- professional/1/4.jpg
- professional/1/5.jpg
- professional/1/6.jpg
- professional/1/7.jpg
- professional/1/8.jpg
img: /files/frames/delicias-flat.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

This restructuration was designed for a couple. The flat has two bedrooms, the main one and the guest room. They key of this flat was the kitchen design. It was divided on two parts with the main corridor between them.

The furniture and finished was chosen to have a minimalism and light space, with a spike wood floor to get a different texture on the floor.
