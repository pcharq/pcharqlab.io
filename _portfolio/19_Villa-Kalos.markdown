---
layout: project
title: Villa Kalos
category:
- Collective Housing
location: Marbella, Spain
size: Plot 3.000 m2, Housing 1.800 m2
carousel:
- professional/14/1.jpg
- professional/14/2.jpg
- professional/14/3.jpg
- professional/14/4.jpg
- professional/14/5.jpg
- professional/14/6.jpg
- professional/14/7.jpg
- professional/14/8.jpg
- professional/14/9.jpg
- professional/14/10.jpg
- professional/14/11.jpg
- professional/14/12.jpg
- professional/14/13.jpg
- professional/14/14.jpg

img: /files/frames/Kalos.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

