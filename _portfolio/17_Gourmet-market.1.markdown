---
layout: project
title: Gourmet <br>Market
category:
- Public Space
location: Marbella, Spain
size: 1.380 m2
carousel:
- professional/5/1.jpg
- professional/5/2.jpg
- professional/5/3.jpg
- professional/5/4.jpg
- professional/5/5.jpg
- professional/5/6.jpg
- professional/5/7.jpg
- professional/5/8.jpg

img: /files/frames/Gourmet-Market.jpg
---

The building is located in the Alfil Building, located in a privilieged area of ​​Marbella, next to Ricardo Soriano Avenue, the main artery of the city. It is due to its excellent location that we are facing a perfect space for the development of a gourmet market.

Taking into account the privilieged situation, two accesses are proposed along the three streets to which it adjoins, in order to have a space that interacts freely with consumers.

The low floor consists of a central element on which the rest of the elements rotate, inccorporating in it the refreshment zone. It also proposes a cafeteria and cocktail area with street access, allowing its operation without the market is open.

In the basement there is a space dedicated to events, conferences and celebrations. The central space is enclosed by a glass curtain depending on the events that take place. On this floor there is also a tasting room and wine tasting with a cellar area