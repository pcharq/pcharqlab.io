---
layout: project
title: Downtown House
category:
- Villa
location: Malaga, Spain
size:  Plot 580m2, Construction 380 m2
carousel:
- professional/23/1.jpg
- professional/23/2.jpeg
- professional/23/3.jpg
- professional/23/4.jpg
- professional/23/5.jpg


img: /files/frames/downtown.jpg
background-img: /files/frames/downtown.jpg
---

Project of a new a Villa in Marbella, Spain