---
layout: project
title: Clavin Klein store
category:
- Retail
location: Madrid, Spain
size: 980 m2
carousel:
- academic/4/1.jpg
- academic/4/2.jpg
- academic/4/3.jpg
- academic/4/4.jpg
- academic/4/5.jpg
- academic/4/6.jpg
- academic/4/7.jpg
- academic/4/8.jpg
- academic/4/9.jpg
- academic/4/10.jpg
- academic/4/11.jpg
- academic/4/12.jpg
- academic/4/13.jpg
img: /files/frames/calvin-klein-store.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

The store connects the two streets that can be accessed to generate communication from the public space. Once inside, double and triple heights are generated to have visual connections with all areas of the building. Inside the store, a route is generated that allows visualizing all the products of the store in a dynamic way, without producing in the consumer a static and boring feeling. 

Materials that match the style of the brand are used, always maintaining a neutral color range that allows the product to be given importance, embedding and emphasizing it.