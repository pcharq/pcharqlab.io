---
layout: project
title: Art & expo center
category:
- Public Building
location: Madrid, Spain
size: 1.575 m2
carousel:
- academic/5/1.jpg
- academic/5/2.jpg
- academic/5/3.jpg
- academic/5/4.jpg
- academic/5/5.jpg
- academic/5/6.jpg
- academic/5/7.jpg
- academic/5/8.jpg
- academic/5/9.jpg
- academic/5/11.jpg
- academic/5/11.jpg
- academic/5/12.jpg
- academic/5/13.jpg
- academic/5/14.jpg
- academic/5/15.jpg
img: /files/frames/art-expo-center.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

The project is located in the old Rex cinemas of the Gran Via of Madrid, a vanished building since 2008. It proposes a completely different configuration to the existing, seeking to be a space complementary to the street, a totally open and public space. 

The existing columns create a natural forest that suggests the creation of a project arising from nature, creating a topographic and organic surface as a staircase where the entire program of the project is located. This program consists of an exposition space, a conference room and a more exclusive space on the first floor, where more special exhibitions will be generated. The materials accompany this search for a complementary and natural space to one of the most important parts of the city