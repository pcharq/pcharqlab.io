---
layout: project
title: Buena Noche <br>Housing
category:
- Collective Housing
location: Estepona, Spain
size: Plot 27.300 m2, Housing 20.202 m2
carousel:
- professional/8/01.jpg
- professional/8/02.jpg
- professional/8/03.jpg
- professional/8/04.jpg



img: /files/frames/buenas-noches.jpg
---

Project of 200 homes located in the area of ​​Buenas Noches, Estepona. It is distributed in two plots with east orientation and views of the sea and the mountains.