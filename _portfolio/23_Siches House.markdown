---
layout: project
title: Siches House
category:
- Villa
location: Malaga, Spain
size:  Plot 560m2, Construction 180 m2
carousel:
- professional/18/1.jpg
- professional/18/2.jpg
- professional/18/3.jpg
- professional/18/4.jpg
- professional/18/5.jpg



img: /files/frames/siches.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

Project of a New Villa in Malaga, Spain