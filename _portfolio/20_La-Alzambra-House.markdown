---
layout: project
title: La Alzambra House
category:
- Villa
location: Marbella, Spain
size: Plot 360 m2, Construction 290 m2
carousel:
- professional/15/1.jpg
- professional/15/2.jpg
- professional/15/3.jpg
- professional/15/4.jpg
- professional/15/5.jpg
- professional/15/6.jpeg
- professional/15/8.jpeg
- professional/15/9.jpeg
- professional/15/12.jpeg
- professional/15/13.jpeg
- professional/15/14.jpeg
- professional/15/15.jpeg
- professional/15/16.jpeg
- professional/15/17.jpeg





img: /files/frames/La-Alzambra.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

Interior refurbishment of a semi-atached house in Marbella.