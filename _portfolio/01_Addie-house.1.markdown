---
layout: project
title: Casa <br>Addie
category:
- Villa
location: Marbella, Spain
size: Plot 1.500 m2, Housing 1.050 m2
carousel:
- professional/9/01.jpg
- professional/9/02.jpg
- professional/9/03.jpg
- professional/9/04.jpg
- professional/9/05.jpg
- professional/9/06.jpg
- professional/9/07.jpg
- professional/9/08.jpg
- professional/9/09.jpg
- professional/9/10.jpg
- professional/9/11.jpg
- professional/9/12.jpg
- professional/9/13.jpg
- professional/9/14.jpg
- professional/9/15.jpg
- professional/9/16.jpg
- professional/9/17.jpg
- professional/9/18.jpg
- professional/9/19.jpg
- professional/9/20.jpg
- professional/9/21.jpg
- professional/9/22.jpg
- professional/9/23.jpg
- professional/9/24.jpg





img: /files/frames/addie.jpg
---

Complete refurbishemnt and extension of a villa in La Quinta golf. In progress