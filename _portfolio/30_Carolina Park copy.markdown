---
layout: project
title: Carolina Park
category:
- Villa
location: Malaga, Spain
size:  Plot 1000m2, Construction 450 m2
carousel:
- professional/25/1.jpg
- professional/25/2.jpeg



img: /files/frames/carolina.jpg
background-img: /files/frames/carolina.jpg
---

New Villa in Marbella, Spain