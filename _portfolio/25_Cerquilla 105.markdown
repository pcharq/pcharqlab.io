---
layout: project
title: Cerquilla 105
category:
- Villa
location: Malaga, Spain
size:  Plot 4500m2, Construction 1080 m2
carousel:
- professional/20/1.jpg
- professional/20/1b.jpg
- professional/20/3.jpg
- professional/20/4.jpg
- professional/20/5.jpg
- professional/20/6.jpg
- professional/20/7.PNG


img: /files/frames/105.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

Project of a New Villa in Marbella, Spain