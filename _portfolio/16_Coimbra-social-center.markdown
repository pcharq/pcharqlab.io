---
layout: project
title: Social center
category:
- Public Building
location: Coimbra, Portugal
size: Plot 86 m2, Construction 260 m2
carousel:
- academic/3/1.jpg
- academic/3/2.jpg
- academic/3/3.jpg
- academic/3/4.jpg
img: /files/frames/coimbra.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

The building is seen as a progressive element able to link all the necessary social elements. Beginning with a public space on the ground floor as a grandstand, which invites pedestrians to access it. 

The vertical communication, due to the size of the plot, is resolved as a part included in the program of uses, having as a culmination, a roof with a 360 degree view