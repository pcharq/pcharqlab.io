---
layout: project
title: Cuatro Caminos flat
category:
- Flat
location: Madrid, Spain
size: 48 m2
carousel:
carousel:
- professional/2/1.jpg
- professional/2/2.jpg
- professional/2/3.jpg
- professional/2/4.jpg
- professional/2/5.jpg
- professional/2/6.jpg
- professional/2/7.jpg
- professional/2/8.jpg
img: /files/frames/cuatro-caminos-flat.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

This restructuration was designed for a single person, who wanted two bed room to rent one of them. Kitchen and Living room are connected to take advantage of the space.

The client wanted to have the bedrooms as much big as posibble with the better orientation.The position of the bathroom and the kitchen are modified slightly to solve easily the facilities
A white color was used to take advantage of the brightness of the apartment, applying grayer shades in the separation between the day zone and the night area.

Oak wood with knots for the floor was proposed to provide rustic warmth and texture to the client's taste
