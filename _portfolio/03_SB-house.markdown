---
layout: project
title: SB <br>House
category:
- Villa
location: Marbella, Spain
size: Plot 830 m2, Housing 340 m2
carousel:
- professional/3/1.jpg
- professional/3/2.jpg
- professional/3/3.jpg
- professional/3/4.jpg
- professional/3/5.jpg
- professional/3/6.jpg
- professional/3/7.jpg
- professional/3/8.jpg
- professional/3/9.jpg
- professional/3/10.jpg
- professional/3/11.jpg
- professional/3/12.jpg
- professional/3/13.jpg
- professional/3/14.jpg
img: /files/frames/sb.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

The client, as owner of the plot, wanted a modern project to be able to market it easily. The plot is located in a privileged place, overlooking the sea and the mountains. It has an infinity pool with a large garden. 

The house is distributed with a large and open day area on the ground floor plus a small office and toilet. On the upper floor, the bedrooms with large windows are proposed taking advantage of the south orientation towards a viewing terrace
