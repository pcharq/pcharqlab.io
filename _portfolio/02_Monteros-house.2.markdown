---
layout: project
title: Monteros <br>House
category:
- Villa
location: Marbella, Spain
size: Plot 1.500 m2, Housing 450 m2
carousel:
- professional/11/01.jpg
- professional/11/02.jpg
- professional/11/03.jpg
- professional/11/04.jpg
- professional/11/05.jpg
- professional/11/06.jpg
- professional/11/07.jpg
- professional/11/08.jpg


img: /files/frames/monteros.jpg
---

New Villa in Los Altos de los monteros, Marbella.