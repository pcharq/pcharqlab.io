---
layout: project
title: Cerquilla House
category:
- Villa
location: Malaga, Spain
size:  Plot 1500m2, Construction 390 m2
carousel:
- professional/19/1.jpg
- professional/19/2.jpg
- professional/19/3.jpg
- professional/19/4.jpg
- professional/19/5.jpg
- professional/19/6.jpg
- professional/19/7.jpg


img: /files/frames/cerquilla.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

Project of a New Villa in Marbella, Spain