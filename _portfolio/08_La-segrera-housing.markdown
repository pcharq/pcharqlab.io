---
layout: project
title: La Segrera housing
category:
- Collective Housing
location: Barcelona, Spain
size: Plot 240.000 m2, Housing 240.000 m2
carousel:
- academic/2/1.jpg
- academic/2/2.jpg
- academic/2/3.jpg
- academic/2/4.jpg
- academic/2/5.jpg
- academic/2/6.jpg
img: /files/frames/la-segrera.jpg
background-img: /academic/1/1.jpg
---

In this proposal, several building typologies are made. Looking for creating spaces that filter privacy. This allow to go from public to the private spaces in a gradual way. 

The buildings are presented as elements built in dry and promoting flexibility housing. As well as sports areas and green spaces are created to be as union spaces between the different buildings