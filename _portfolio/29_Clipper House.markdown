---
layout: project
title: Clipper House
category:
- Villa
location: Malaga, Spain
size:  Plot 580m2, Construction 380 m2
carousel:
- professional/24/1.jpg
- professional/24/2.jpeg
- professional/24/3.jpeg
- professional/24/4.jpeg
- professional/24/5.jpeg



img: /files/frames/clipper.jpg
background-img: /files/frames/clipper.jpg
---

New Villa in Marbella, Spain