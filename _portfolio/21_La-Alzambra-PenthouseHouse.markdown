---
layout: project
title: La Alzambra PentHouse
category:
- Duplex
location: Marbella, Spain
size:  Construction 210 m2
carousel:
- professional/16/1.jpg
- professional/16/2.jpg
- professional/16/2B.jpg
- professional/16/2C.jpg
- professional/16/3.jpeg
- professional/16/4.jpeg
- professional/16/5.jpeg
- professional/16/6.jpg
- professional/16/7.jpeg
- professional/16/8.jpeg


img: /files/frames/pent.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

Interior refurbishment of a duplex penthouse in Marbella.