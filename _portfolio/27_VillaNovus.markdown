---
layout: project
title: Villa Novus
category:
- Villa
location: Malaga, Spain
size:  Plot 2900m2, Construction 580 m2
carousel:
- professional/22/1.jpg
- professional/22/2.jpeg
- professional/22/3.jpg
- professional/22/4.jpg
- professional/22/5.jpg
- professional/22/6.jpg
- professional/22/7.jpg
- professional/22/8.jpg


img: /files/frames/novus.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

Full renovation project for a Villa in Marbella, Spain