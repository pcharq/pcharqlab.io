---
layout: project
title: Vasari <br>House
category:
- Twin House
location: Marbella, Spain
size: Plot 950 m2, Housing 470 m2
carousel:
- professional/10/01.jpg
- professional/10/02.jpg
- professional/10/03.jpg
- professional/10/04.jpg
- professional/10/05.jpg
- professional/10/06.jpg
- professional/10/07.jpg
- professional/10/08.jpg
- professional/10/09.jpg
- professional/10/10.jpg


img: /files/frames/vasari.jpg
---

Complete refurbishemnt of a villa in La Puerto Banús. In progress