---
layout: project
title: Villa Grilly
category:
- Villa
location: Grilly, France
size:  Plot 650m2, Construction 360 m2
carousel:
- professional/17/1.jpg
- professional/17/2.jpg
- professional/17/3.jpg
- professional/17/4.jpg
- professional/17/5.jpg
- professional/17/6.jpg
- professional/17/7.jpg
- professional/17/8.jpg
- professional/17/9.jpg


img: /files/frames/grilly.jpg
background-img: /files/frames/art_expo_center_frame.jpg
---

Project of a New Villa in Grily, France