# frozen_string_literal: true

Gem::Specification.new do |spec|
    spec.name          = "pcharq.gitlab.io"
    spec.version       = "1.0.0"
    spec.authors       = ["Javissimo"]
    spec.email         = ["javier@tuta.io"]
  
    spec.summary       = "Another Jekyll theme"
    spec.homepage      = "https://gitlab.com/pcharq/pcharq.gitlab.io"
    spec.license       = "MIT"
  
    spec.metadata["plugin_type"] = "theme"
  
    spec.files = `git ls-files -z`.split("\x0").select do |f|
      f.match(%r!^(assets|_(includes|layouts|sass)/|(LICENSE|README)((\.(txt|md|markdown)|$)))!i)
    end

    spec.add_runtime_dependency "jekyll", ">= 4.0", "< 5.0"
    spec.add_runtime_dependency "jekyll-paginate-v2", ">= 2.0.0"
  
    spec.add_development_dependency "bundler"
  end
  