# Cómo ver la web en local

1. Abrir una ventana de comandos en la carpeta raíz
2. Escribir lo siguiente:
   jekyll s --watch 
3. Abrir una pestaña en el navegador y entrar en la siguiente dirección
localhost:4000

# Cómo subir los cambios

1. Realizar los cambios
2. Añadir y "aplicar" los cambios:
git commit -am "Mensaje explicativo de lo que he hecho"
3. Subir los cambios:
git push
